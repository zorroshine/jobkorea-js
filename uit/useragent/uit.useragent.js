(function(root, factory) {
	if (typeof define === 'function' && define.amd) {
		define(function() {
			return factory(root);
		});
	} else if (typeof exports === 'object') {
		module.exports = factory;
	} else {
		root.uit  = root.uit || {};
		if (typeof Object.assign == 'function') {
			// console.log('assign');
			root.uit = Object.assign(root.uit, factory(root));
		} else if (typeof jQuery != 'undefined') {
			// console.log('jQuery');
			root.uit = jQuery.extend(root.uit, factory(root));
		} else {
			// console.log('none');
			var extend = function() {
				var extended = {};
				for(key in arguments) {
					var argument = arguments[key];
					for (prop in argument) {
						if (Object.prototype.hasOwnProperty.call(argument, prop)) {
							extended[prop] = argument[prop];
						}
					}
				}
				return extended;
			};
			root.uit = extend(root.uit, factory(root));
		}
	}
})(this, function(root) {

	'use strict';

	var core = {};

	// UserAgent
	function detect(ua) {

		function getFirstMatch(regex) {
			var match = ua.match(regex);
			return (match && match.length > 1 && match[1]) || '';
		}

		function getSecondMatch(regex) {
			var match = ua.match(regex);
			return (match && match.length > 1 && match[2]) || '';
		}

		var iosdevice = getFirstMatch(/(ipod|iphone|ipad)/i).toLowerCase(),
			likeAndroid = /like android/i.test(ua),
			android = !likeAndroid && /android/i.test(ua),
			nexusMobile = /nexus\s*[0-6]\s*/i.test(ua),
			nexusTablet = !nexusMobile && /nexus\s*[0-9]+/i.test(ua),
			chromeos = /CrOS/.test(ua),
			silk = /silk/i.test(ua),
			sailfish = /sailfish/i.test(ua),
			tizen = /tizen/i.test(ua),
			webos = /(web|hpw)os/i.test(ua),
			windowsphone = /windows phone/i.test(ua),
			samsungBrowser = /SamsungBrowser/i.test(ua),
			windows = !windowsphone && /windows/i.test(ua),
			mac = !iosdevice && !silk && /macintosh/i.test(ua),
			linux = !android && !sailfish && !tizen && !webos && /linux/i.test(ua),
			edgeVersion = getFirstMatch(/edge\/(\d+(\.\d+)?)/i),
			versionIdentifier = getFirstMatch(/version\/(\d+(\.\d+)?)/i),
			tablet = /tablet/i.test(ua),
			mobile = !tablet && /[^-]mobi/i.test(ua),
			xbox = /xbox/i.test(ua),
			result;
		var osVersion = '';

		if (/opera/i.test(ua)) {
			//  an old Opera
			result = {
				name: 'Opera',
				opera: true,
				version: versionIdentifier || getFirstMatch(/(?:opera|opr|opios)[\s\/](\d+(\.\d+)?)/i)
			};
		} else if (/opr|opios/i.test(ua)) {
			// a new Opera
			result = {
				name: 'Opera',
				opera: true,
				version: getFirstMatch(/(?:opr|opios)[\s\/](\d+(\.\d+)?)/i) || versionIdentifier
			};
		} else if (/SamsungBrowser/i.test(ua)) {
			result = {
				name: 'Samsung Internet for Android',
				samsungBrowser: true,
				version: versionIdentifier || getFirstMatch(/(?:SamsungBrowser)[\s\/](\d+(\.\d+)?)/i)
			};
		} else if (/coast/i.test(ua)) {
			result = {
				name: 'Opera Coast',
				coast: true,
				version: versionIdentifier || getFirstMatch(/(?:coast)[\s\/](\d+(\.\d+)?)/i)
			};
		} else if (/yabrowser/i.test(ua)) {
			result = {
				name: 'Yandex Browser',
				yandexbrowser: true,
				version: versionIdentifier || getFirstMatch(/(?:yabrowser)[\s\/](\d+(\.\d+)?)/i)
			};
		} else if (/ucbrowser/i.test(ua)) {
			result = {
				name: 'UC Browser',
				ucbrowser: true,
				version: getFirstMatch(/(?:ucbrowser)[\s\/](\d+(?:\.\d+)+)/i)
			};
		} else if (/mxios/i.test(ua)) {
			result = {
				name: 'Maxthon',
				maxthon: true,
				version: getFirstMatch(/(?:mxios)[\s\/](\d+(?:\.\d+)+)/i)
			};
		} else if (/epiphany/i.test(ua)) {
			result = {
				name: 'Epiphany',
				epiphany: true,
				version: getFirstMatch(/(?:epiphany)[\s\/](\d+(?:\.\d+)+)/i)
			};
		} else if (/puffin/i.test(ua)) {
			result = {
				name: 'Puffin',
				puffin: true,
				version: getFirstMatch(/(?:puffin)[\s\/](\d+(?:\.\d+)?)/i)
			};
		} else if (/sleipnir/i.test(ua)) {
			result = {
				name: 'Sleipnir',
				sleipnir: true,
				version: getFirstMatch(/(?:sleipnir)[\s\/](\d+(?:\.\d+)+)/i)
			};
		} else if (/k-meleon/i.test(ua)) {
			result = {
				name: 'K-Meleon',
				kMeleon: true,
				version: getFirstMatch(/(?:k-meleon)[\s\/](\d+(?:\.\d+)+)/i)
			};
		} else if (windowsphone) {
			result = {
				name: 'Windows Phone',
				windowsphone: true
			};
			if (edgeVersion) {
				result.msedge = true;
				result.version = edgeVersion;
			} else {
				result.msie = true;
				result.version = getFirstMatch(/iemobile\/(\d+(\.\d+)?)/i);
			}
		} else if (/msie|trident/i.test(ua)) {
			result = {
				name: 'Internet Explorer',
				msie: true,
				version: getFirstMatch(/(?:msie |rv:)(\d+(\.\d+)?)/i)
			};
		} else if (chromeos) {
			result = {
				name: 'Chrome',
				chromeos: true,
				chromeBook: true,
				chrome: true,
				version: getFirstMatch(/(?:chrome|crios|crmo)\/(\d+(\.\d+)?)/i)
			};
		} else if (/chrome.+? edge/i.test(ua)) {
			result = {
				name: 'Microsoft Edge',
				msedge: true,
				version: edgeVersion
			};
		} else if (/vivaldi/i.test(ua)) {
			result = {
				name: 'Vivaldi',
				vivaldi: true,
				version: getFirstMatch(/vivaldi\/(\d+(\.\d+)?)/i) || versionIdentifier
			};
		} else if (sailfish) {
			result = {
				name: 'Sailfish',
				sailfish: true,
				version: getFirstMatch(/sailfish\s?browser\/(\d+(\.\d+)?)/i)
			};
		} else if (/seamonkey\//i.test(ua)) {
			result = {
				name: 'SeaMonkey',
				seamonkey: true,
				version: getFirstMatch(/seamonkey\/(\d+(\.\d+)?)/i)
			};
		} else if (/firefox|iceweasel|fxios/i.test(ua)) {
			result = {
				name: 'Firefox',
				firefox: true,
				version: getFirstMatch(/(?:firefox|iceweasel|fxios)[ \/](\d+(\.\d+)?)/i)
			};
			if (/\((mobile|tablet);[^\)]*rv:[\d\.]+\)/i.test(ua)) {
				result.firefoxos = true;
			}
		} else if (silk) {
			result =  {
				name: 'Amazon Silk',
				silk: true,
				version : getFirstMatch(/silk\/(\d+(\.\d+)?)/i)
			};
		} else if (/phantom/i.test(ua)) {
			result = {
				name: 'PhantomJS',
				phantom: true,
				version: getFirstMatch(/phantomjs\/(\d+(\.\d+)?)/i)
			};
		} else if (/slimerjs/i.test(ua)) {
			result = {
				name: 'SlimerJS',
				slimer: true,
				version: getFirstMatch(/slimerjs\/(\d+(\.\d+)?)/i)
			};
		} else if (/blackberry|\bbb\d+/i.test(ua) || /rim\stablet/i.test(ua)) {
			result = {
				name: 'BlackBerry',
				blackberry: true,
				version: versionIdentifier || getFirstMatch(/blackberry[\d]+\/(\d+(\.\d+)?)/i)
			};
		} else if (webos) {
			result = {
				name: 'WebOS',
				webos: true,
				version: versionIdentifier || getFirstMatch(/w(?:eb)?osbrowser\/(\d+(\.\d+)?)/i)
			};
		} else if (/bada/i.test(ua)) {
			result = {
				name: 'Bada',
				bada: true,
				version: getFirstMatch(/dolfin\/(\d+(\.\d+)?)/i)
			};
		} else if (tizen) {
			result = {
				name: 'Tizen',
				tizen: true,
				version: getFirstMatch(/(?:tizen\s?)?browser\/(\d+(\.\d+)?)/i) || versionIdentifier
			};
		} else if (/qupzilla/i.test(ua)) {
			result = {
				name: 'QupZilla',
				qupzilla: true,
				version: getFirstMatch(/(?:qupzilla)[\s\/](\d+(?:\.\d+)+)/i) || versionIdentifier
			};
		} else if (/chromium/i.test(ua)) {
			result = {
				name: 'Chromium',
				chromium: true,
				version: getFirstMatch(/(?:chromium)[\s\/](\d+(?:\.\d+)?)/i) || versionIdentifier
			};
		} else if (/chrome|crios|crmo/i.test(ua)) {
			result = {
				name: 'Chrome',
				chrome: true,
				version: getFirstMatch(/(?:chrome|crios|crmo)\/(\d+(\.\d+)?)/i)
			};
		} else if (android) {
			result = {
				name: 'Android',
				version: versionIdentifier
			};
		} else if (/safari|applewebkit/i.test(ua)) {
			result = {
				name: 'Safari',
				safari: true
			};
			if (versionIdentifier) {
				result.version = versionIdentifier;
			}
		} else if (iosdevice) {
			result = {
				name : iosdevice == 'iphone' ? 'iPhone' : iosdevice == 'ipad' ? 'iPad' : 'iPod'
			};
			// WTF: version is not part of user agent in web apps
			if (versionIdentifier) {
				result.version = versionIdentifier;
			}
		} else if(/googlebot/i.test(ua)) {
			result = {
				name: 'Googlebot',
				googlebot: true,
				version: getFirstMatch(/googlebot\/(\d+(\.\d+))/i) || versionIdentifier
			};
		} else {
			result = {
				name: getFirstMatch(/^(.*)\/(.*) /),
				version: getSecondMatch(/^(.*)\/(.*) /)
			};
		}

		// set webkit or gecko flag for browsers based on these engines
		if (!result.msedge && /(apple)?webkit/i.test(ua)) {
			if (/(apple)?webkit\/537\.36/i.test(ua)) {
				result.name = result.name || "Blink";
				result.blink = true;
			} else {
				result.name = result.name || "Webkit";
				result.webkit = true;
			}
			if (!result.version && versionIdentifier) {
				result.version = versionIdentifier;
			}
		} else if (!result.opera && /gecko\//i.test(ua)) {
			result.name = result.name || "Gecko";
			result.gecko = true;
			result.version = result.version || getFirstMatch(/gecko\/(\d+(\.\d+)?)/i);
		}

		// set OS flags for platforms that have multiple browsers
		if (!result.windowsphone && !result.msedge && (android || result.silk)) {
			result.android = true;
			result.osname = 'Android';
		} else if (!result.windowsphone && !result.msedge && iosdevice) {
			result[iosdevice] = true;
			result.ios = true;
			result.osname = iosdevice;
		} else if (mac) {
			result.mac = true;
			result.osname = 'Mac';
		} else if (xbox) {
			result.xbox = true;
			result.osname = 'Xbox';
		} else if (windows) {
			result.windows = true;
			result.osname = 'Windows';
		} else if (linux) {
			result.linux = true;
			result.osname = 'Linux';
		}

		function getWindowsVersion(s) {
			switch (s) {
				case 'NT': return 'NT';
				case 'XP': return 'XP';
				case 'NT 5.0': return '2000';
				case 'NT 5.1': return 'XP';
				case 'NT 5.2': return '2003';
				case 'NT 6.0': return 'Vista';
				case 'NT 6.1': return '7';
				case 'NT 6.2': return '8';
				case 'NT 6.3': return '8.1';
				case 'NT 10.0': return '10';
				default: return undefined;
			}
		}
		
		// OS version extraction
		if (result.windows) {
			osVersion = getWindowsVersion(getFirstMatch(/Windows ((NT|XP)( \d\d?.\d)?)/i));
		} else if (result.windowsphone) {
			osVersion = getFirstMatch(/windows phone (?:os)?\s?(\d+(\.\d+)*)/i);
		} else if (result.mac) {
			osVersion = getFirstMatch(/Mac OS X (\d+([_\.\s]\d+)*)/i);
			osVersion = osVersion.replace(/[_\s]/g, '.');
		} else if (iosdevice) {
			osVersion = getFirstMatch(/os (\d+([_\s]\d+)*) like mac os x/i);
			osVersion = osVersion.replace(/[_\s]/g, '.');
		} else if (android) {
			osVersion = getFirstMatch(/android[ \/-](\d+(\.\d+)*)/i);
		} else if (result.webos) {
			osVersion = getFirstMatch(/(?:web|hpw)os\/(\d+(\.\d+)*)/i);
		} else if (result.blackberry) {
			osVersion = getFirstMatch(/rim\stablet\sos\s(\d+(\.\d+)*)/i);
		} else if (result.bada) {
			osVersion = getFirstMatch(/bada\/(\d+(\.\d+)*)/i);
		} else if (result.tizen) {
			osVersion = getFirstMatch(/tizen[\/\s](\d+(\.\d+)*)/i);
		}
		if (osVersion) {
			result.osversion = osVersion;
		}

		// device type extraction
		var osMajorVersion = !result.windows && osVersion.split('.')[0];
		if (tablet || nexusTablet || iosdevice == 'ipad' || (android && (osMajorVersion == 3 || (osMajorVersion >= 4 && !mobile))) || result.silk) {
			result.tablet = true;
		} else if (mobile || iosdevice == 'iphone' || iosdevice == 'ipod' || android || nexusMobile || result.blackberry || result.webos || result.bada) {
			result.mobile = true;
		}

		result.info = result.osname + ' ' + result.osversion + ', ' + result.name + ' ' + result.version;

		return result;
	}

	var browser = core.browser = detect(typeof navigator !== 'undefined' ? navigator.userAgent || '' : '');

	browser.test = function(browserList) {
		for (var i = 0; i < browserList.length; ++i) {
			var browserItem = browserList[i];
			if (typeof browserItem === 'string') {
				if (browserItem in browser) {
					return true;
				}
			}
		}
		return false;
	};

	function getMajorVersion(version) {
		return version.split(".")[0];
	}

	browser.bodyAddClass = function(option) { // option default: all, 1:browser only, 2: os only
		var classNames = '';
		if ((option == null)||(option == 1)) {
			if ((browser.msie)||(browser.msedge)) {
				classNames += ' is-ie';
				classNames += ' is-ie'+getMajorVersion(browser.version);
			} else if (browser.chrome) {
				classNames += ' is-chrome';
			} else if (browser.firefox) {
				classNames += ' is-firefox';
			}
		}
		if ((option == null)||(option == 2)) {
			if (browser.windows) {
				classNames += ' is-windows';
				classNames += ' is-windows'+getMajorVersion(browser.osversion);
			} else if (browser.android) {
				classNames += ' is-android';
				classNames += ' is-android'+getMajorVersion(browser.osversion);
			} else if (browser.ios) {
				classNames += ' is-ios';
				classNames += ' is-ios'+getMajorVersion(browser.osversion);
			}
		}
		document.getElementsByTagName('body')[0].className += classNames;

		return classNames;
	};

	return core;
});
