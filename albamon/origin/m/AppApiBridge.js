﻿/**
    * Mobile Application API Wrapper
    *
    * AppApiBrider.AppNo, AppVer, AppVerRP 값 할당 필수
    * TODO : 향후 /js/AlbamonCommon.js 편입 고려
    **/
var AppApiBridge = {
    AppNo: null,
    AppVer: null,
    AppVerRP: null,
    MenuInfoURL: "/app/albamon/mon_menu_info.asp",
    /**
    * 초기화
    **/
    Initialize: function (appNo, appVer, appVerRP, menuInfoURL) {
        this.AppNo = appNo;
        this.AppVer = appVer;
        this.AppVerRP = appVerRP;
        this.MenuInfoURL = menuInfoURL;
    },
    MobileMenuInfo: null,
    getCurrentHostURL: function () {
        var result = window.location.protocol + "//" + window.location.hostname;
        return result;
    },
    getHttpUrl: function () {
        var result = "https://" + window.location.hostname;
        if (window.location.hostname.indexOf("dev-") >= 0)
            result = "http://" + window.location.hostname;

        var port = "";
        if (window.location.hostname.indexOf("m19") >= 0) port = ":444";
        result += port;
        return result;
    },
    /**
    * 필수 회원 정보가 존재하지 않는 회원에 대한,
    * 메세지 표시 및 회원정보 수정 페이지 이동
    **/
    goToMemberInfoModifyBySimpleUser: function (isSocial) {
        isSocial = (isSocial == undefined || isSocial == null) ? false : isSocial;

        alert("이력서 등록을 위해 필요한 필수 회원정보가 있습니다.\n정보 입력을 위해 회원정보 수정 페이지로 이동합니다.");
        if (this.AppNo == "17") {
            if (isSocial) {
                this.goPageNo(205, "", this.getHttpUrl() + "/mem/mon_mem_info.asp");
            } else {
                this.goPageNo(205);
            }
        }

        var url = this.getHttpUrl() + "/mem/mon_mem_info_pwd.asp";
        if (isSocial) {
            url = this.getHttpUrl() + "/mem/mon_mem_info.asp";
        }

        if (this.AppNo == "18") {
            location.href = "toapp:popup_close[&]close_type[=]3[&]url[=]" + url + "[&]title[=]" + "회원정보";
        } else {
            location.href = url;
        }
    },

    //회원정보 수정 바로가기  //새창으로 띄우기
    goToMemberInfoModify: function (isSocial) {
        var url = "/mem/mon_mem_info_pwd.asp";
        url = (isSocial) ? "/mem/mon_mem_info.asp" : "/mem/mon_mem_info_pwd.asp";
        url = this.getHttpUrl() + url;

        if (AM_App_No == "17") {
            window.android.openPopup(url, "회원정보");
        } else if (AM_App_No == "18") {
            if (AM_App_Ver_RP < "50104") {
                document.location = "toapp:popup[&]url[=]" + url + "[&]title[=]회원정보[&]title_type[=]1";
            }
            else {
                document.location = "toapp:popup[&]url[=]" + url + "[&]title[=]회원정보";
            }
        } else {
            document.location.href = url;
        }
    },
    /**
    * 스크랩 목록으로 이동
    * 링크 수정 완료
    */
    goToScrapList: function () {
        if (this.AppNo == "17" || this.AppNo == "18") {
            this.goPageNo(101, null);
        }
        else {
            location.href = "/list/gi/mon_scrap_list.asp";
        }
    },
    /**
    * 최근 지원 목록으로 이동
    * 링크 수정 완료
    */
    goToRecentList: function () {
        if (this.AppNo == "17" || this.AppNo == "18") {
            this.goPageNo(102, null);
        }
        else {
            location.href = "/list/gi/mon_recent_list.asp";
        }
    },
    /**
    * 근로계약서로 목록으로 이동
    * 링크 수정 완료
    **/
    goToContractList: function () {
        var url = "/contract/list/ws1";
        if (this.AppNo == "17") {
            window.android.openPopup(url, '근로계약서 관리');
        } else if (this.AppNo == "18") {
            app_popup(url, '근로계약서 관리', '');
        } else {
            location.href = url;
        }
    },
    /**
    * 근로계약서로 이동
    **/
    goToContract: function (al_gi_no) {

        var url = "/Contract?GI_No=" + al_gi_no;

        if (al_gi_no != '') {
            if (this.AppNo == "17") {
                window.android.openPopup(url, '근로계약서');
            } else if (this.AppNo == "18") {
                app_popup(url, '근로계약서', '');
            } else {
                location.href = url;
            }
        }
    },
    /*
    * 지원현황으로 새창 이동
    * 링크 수정 완료
    */
    openApplyMain: function () {
        this.openPopup("/Member/ApplyManager", "지원현황", "", "", 0);
    },
    
    /*
    * 이력서 관리로 새창 이동
    * 링크 수정 완료
    */
    openResumeManagement: function () {
        //this.openPopup("/mem/mon_resume_edit_view.asp?parent_reflesh=y", "이력서관리", "", "refresh_pop=1", 0);
        this.openPopup("/resume/manager?parent_reflesh=y", "이력서관리", "", "refresh_pop=1", 0);
    },
    /**
    * 이력서 작성하기 랜딩 페이지로 새창 이동
    * 링크 수정 완료
    **/
    openResumeWriteLanding: function (referpage, parentReflesh) {
        referpage = (referpage == undefined || referpage == null) ? "mypage" : referpage;
        parentReflesh = (parentReflesh == undefined || parentReflesh == null) ? false : parentReflesh;
        var url = "/mem/resume/mon_resume_main.asp?referpage=" + referpage;
        var param = "referpage=" + referpage;
        if (parentReflesh)
            param += "&refresh_pop=1";

        this.openPopup(url, "이력서작성", "", param, 0);
    },
    /**
    * 임시저장된 이력서 작성으로 새창 이동
    * 링크 수정 완료
    **/
    openResumeWriteTemp: function () {
        this.openResume("/Resume/ResumeTempWrite", "임시저장된 이력서 작성");
    },
    /**
    * 이력서 공개 관리로 새창 이동
    * 링크 수정 완료
    **/
    openResumeOpenManagement: function (requestLocation) {
        requestLocation = (requestLocation == undefined || requestLocation == null) ? "" : requestLocation;
        var url = "/Resume/ResumeStat?RequestLocation=" + requestLocation;
        if (this.AppNo == "17" || this.AppNo == "18") {
            this.openPopup(url, "이력서 공개설정");
        } else {
            location.href = url;
        }
    },
    /**
    * 열람제한 기업등록(검색)
    **/
    openResumeLimitCoprSearch : function()
    {
        this.openPopup("/member/mon_limit_search?parent_reflesh=y", "열람제한 기업등록", "", "refresh_pop=1", 0);
    }
    ,
    /**
    * 이력서 열람 기업으로 새창 이동
    * 링크 수정 완료
    */
    openResumeOpen: function (initab, clostype) {
        initab = (initab == undefined || initab == null) ? 0 : initab;
        clostype = (clostype == undefined || clostype == null) ? 1 : clostype;

        var url1 = "/member/mon_resume_open?parent_reflesh=y&from_javascript=y";
        var url2 = "/member/mon_resume_limit?parent_reflesh=y&from_javascript=y";
        var url = (initab == 0) ? url1 : url2;

        if (this.AppNo == "18") {
            this.toAppExecute("popup[&]url[=]" + url + "[&]title[=]내 이력서 열람기업[&]param[=][&]tab_names[=]내 이력서 열람기업;열람제한기업[&]tab_urls[=]" + url1 + ";" + url2 + "[&]tab_selectIdx[=]" + initab + ")");
        } else {
            try{
                window.android.openTabPopup("내 이력서 열람기업", "내 이력서 열람기업;열람제한기업", url1 + ";" + url2, initab, clostype);
            } catch (e) {
                if(this.AppNo != "17")
                {
                    this.openPopup(url, "내 이력서 열람기업", "", "refresh_pop=1", 0);
                }
            }
        }
    },
    /**
    * 관심기업으로 새창 이동
    * 링크 수정 완료
    */
    openFavorCorp: function (initab, clostype) {
        initab = (initab == undefined || initab == null) ? 0 : initab;
        clostype = (clostype == undefined || clostype == null) ? 1 : clostype;

        var url1 = "/member/mon_favor_corp";
        var url2 = "/member/mon_favor_corp_recruit_list";
        var url = (initab == 0) ? url1 : url2;

        if (this.AppNo == "18") {
            this.toAppExecute("popup[&]url[=]" + url + "[&]title[=]관심기업[&]param[=][&]tab_names[=]관심기업 목록;관심기업 알바정보[&]tab_urls[=]" + url1 + ";" + url2 + "[&]tab_selectIdx[=]" + initab + ")");
        } else {
            try{
                window.android.openTabPopup("관심기업", "관심기업 목록;관심기업 알바정보", url1 + ";" + url2, initab, clostype);
            } catch (e) {
                if(this.AppNo != "17")
                {
                    this.openPopup(url, "내 이력서 열람기업", "", "refresh_pop=1", 0);
                }
            }
        }
    },
    /**
    * 회원 정보 수정으로 새창 이동
    * 링크 수정 완료
    **/
    openMemInfo: function () {
        this.openPopup("/mem/mon_mem_info_main.asp", "회원정보");
    },

    /**
    * 회원 정보 - 개인정보 수정으로 새창 이동
    **/
    openMemInfoEidt: function () {
        var url = "/mem/mon_mem_info.asp";
        if (this.AppNo == "17" || this.AppNo == "18") {
            this.openPopup("/mem/mon_mem_info.asp", "회원정보");
        } else {
            location.href = url;
        }
    },

    /**
    * 회원 가입 
    **/
    openMemberRegist: function (returnUrl) {
        returnUrl = (returnUrl == undefined || returnUrl == null) ? "/" : returnUrl;
        if (this.AppNo == "17") {
            window.android.openMemberRegist();
        } else {
            this.openPopup("/login/memberShip/agree_gg.asp?re_url=" + returnUrl, "개인회원가입", "", "refresh_pop=1", 0);
        }
    },
    /**
    * 개인 회원 탈퇴
    **/
    openMemberLeave: function () {
        var url = "/account/leave/member";
        if (this.AppNo == "17" || this.AppNo == "18") {
            this.openPopup(url, "회원탈퇴", "", "", 0);
        } else {
            location.href = url;
        }
    },
    /**
    * 기업 회원 가입 
    **/
    openCorpRegist: function (returnUrl) {
        returnUrl = (returnUrl == undefined || returnUrl == null) ? "/" : returnUrl;
        var url = "/login/memberShip/agree_gi.asp?re_url=" + returnUrl;
        if (this.appNo == "17" || this.appNo == "18") {
            this.openPopup(url, "알바몬ㆍ잡코리아 통합 회원가입", "SSL", "refresh_pop=1", 0);
        } else {
            location.href = url;
        }
    },
    /**
     * 기업 회원 정보 수정 
     **/
    openCorpEdit: function () {
        var url = "/corp/mon_co_info_edit.asp";
        if (this.AppNo == "17" || this.AppNo == "18") {
            this.openPopup(url, "회원정보 수정", "SSL", "", 0);
        } else {
            location.href = url;
        }
    },
    /**
    * 기업 회원 뉴스레터·문자수신 설정
    **/
    openNewsLetter: function () {
        var url = "/account/corp/news-info";
        if (this.AppNo == "17" || this.AppNo == "18") {
            this.openPopup(url, "뉴스레터·문자수신 설정", "SSL", "", 0);
        } else {
            location.href = url;
        }
    },
    /**
    * 기업 회원 탈퇴
    **/
    openCorpLeave: function () {
        var url = "/account/leave/corporation";
        if (this.AppNo == "17" || this.AppNo == "18") {
            this.openPopup(url, "회원탈퇴", "", "", 0);
        } else {
            location.href = url;
        }
    },
    /**
    * 로그인 팝업 호출
    **/
    loginPop: function (returnUrl) {
        returnUrl = (returnUrl == undefined || returnUrl == null) ? "/" : returnUrl;
        if (this.AppNo == "17") {
            window.android.login_pop();
        }
        else if (this.AppNo == "18" && this.AppVerRP >= "50102") {
            this.toAppExecute("login_pop_type");
        }
        else {
            location.href = "/login/login.asp?re_url=" + returnUrl;
        }
    },
    /**
    * 특정 회원종류 로그인만 지원하는 로그인 팝업 호출
    * type : 0 - 개인회원, 1 - 기업회원
    **/
    loginPopupType: function (type, returnUrl) {
        returnUrl = (returnUrl == undefined || returnUrl == null) ? "/" : returnUrl;
        if (this.AppNo == "17") {
            window.android.login_pop_onlyType(type);
        }
        else if (this.AppNo == "18" && this.AppVerRP >= "50102") {
            this.toAppExecute("login_pop_type[&]type[=]" + type);
        }
        else {
            location.href = "/login/login.asp?re_url=" + returnUrl;
        }
    },
    /**
    * 로그아웃 
    **/
    logout: function (isGoDirectRoot) {
        isGoDirectRoot = (isGoDirectRoot == isGoDirectRoot || isGoDirectRoot == null) ? false : isGoDirectRoot;
        if (this.AppNo == "17") {
            if (this.AppVerRP < "30002")
                location.href = "/app/albamon/call_logout_script_android.asp";
            else
                window.android.logout();
        } else if (this.AppNo == "18") {
            this.toAppExecute("logout");
        } else {
            location.href = "/login/logout.asp";
        }
    },
    /**
    * 채용정보 상세 이동
    **/
    goGibRead: function (gGi_No, url, param, reflesh) {
        url = (url == undefined || url == null) ? "" : url;
        param = (param == undefined || param == null) ? "" : param;
        reflesh = (reflesh == undefined || reflesh == null) ? false : reflesh;

        if (!(this.AppNo == "18" && this.AppVerRP <= "50105")) {
            url = url.replace("/list/gi/mon_gib_read.asp", "/recruit/view/gi");
        }

        if (this.AppNo == "17") {
            if (param != "") {
                if (url != "") {
                    //url 공고번호 
                    if (url.substring(url.length - 1, url.length).toLowerCase() != "=" && url.indexOf("&tmpv=") < 0) {
                        url += ((url.indexOf("?") < 0) ? "?tmpv=" : "&tmpv=");
                    }
                    if (reflesh != false) {
                        window.android.goGibRead(gGi_No, param, url, true);
                    } else {
                        window.android.goGibRead(gGi_No, param, url);
                    }
                } else {
                    window.android.goGibRead(gGi_No, param);
                }
            }
            else {
                window.android.goGibRead(gGi_No);
            }
        }
        else {
            if (reflesh == true) {
                this.openPopup(url + "&parent_reflesh=y", '채용정보', "", "refresh_pop=1", 0);
            } else {
                this.openPopup(url, '채용정보');
            }
        }
    },
    goGibReadLinkAddClass: function (obj) {  //공고 선택시 리스트 컬러 변경
        if (this.AppNo == "18") {
            obj.addClass("visited");
        }
    },
    /**
    * 이력서 작성하기
    **/
    openResume: function (url, title) {
        title = (title == undefined || title == null) ? "이력서 작성" : title;
        if (this.AppNo == "17") {
            window.android.open_resume(url);
        }
        else if (this.AppNo == "18" && this.AppVerRP < "50104") {
            this.openPopup(url, title, "", "", "1");
        }
        else {
            this.openPopup(url, title);
        }
    },
    /**
    * 이력서 수정하기
    **/
    openResumeEdit: function (resumeIndex, url, title) {
        title = (title == undefined || title == null) ? "이력서 수정" : title;
        if (this.AppNo == "17") {
            window.android.open_resume_edit(resumeIndex, url);
        }
        else if (this.AppNo == "18" && this.AppVerRP < "50104") {
            this.openPopup(url, title, "", "", "1");
        }
        else {
            this.openPopup(url, title);
        }
    },
    /*
    * 기업 홈으로 이동
    */
    goCorpHome: function () {
        var url = '/recruitmanager';
        if (this.AppNo == "17" || this.AppNo == "18") {
            this.goPageNo(501, null);
        }
        else {
            location.href = url;
        }
    },
    /*
    * 기업 정보 홈으로 이동
    */
    goCorpInfoHome: function () {
        if (this.AppNo == "17" || this.AppNo == "18") {
            this.goPageNo(509, null);
        }
        else {

            location.href = "/corp/mon_co_info_main.asp";
        }
    },
    /*
    * 기업 > 공고관리 이동
    * type 0 : 게재중, 1: 대기중, 2: 마감, 3: 관리자 삭제
    */
    goRecruitManager: function (type, param) {
        param = (param == undefined || param == null) ? "" : param;
        var goUrl = "/recruitmanager/list-";
        if (type == 1) goUrl += "open";
        else if (type == 2) goUrl += "ready";
        else if (type == 3) goUrl += "close";
        else if (type == 4) goUrl += "deleted-admin";
        else goUrl += "open";
        if (this.AppNo == "17" || this.AppNo == "18") {
            this.goPageNo(502, param, goUrl);
        } else {
            location.href = goUrl;
        }
    },
    /*
    * 기업 > 유료이용내역 이동
    */
    goPayHistory: function (algino) {
        var url = "/RecruitManager/ChargedHistory/SingleRecruitList?AL_GI_No=";
        url += algino;
        if (this.AppNo == "17" || this.AppNo == "18") {
            this.openPopup(url, "유료이용내역", "SSL", "refresh_pop=1", 0);
        }
        else {
            location.href = url;
        }
    },
    /*
    * 기업 > 유료이용내역 - 이머니 이동
    */
    goCorpEMoneyHistory: function () {
        var menuNo = 5120101;
        var url = "/RecruitManager/ChargedHistory/EmoneyAdd";
        this.goPageNo(menuNo, "", "");
    },
    /*
    * 기업 > 유료이용내역 - 충전금 이동
    */
    goCorpMonCachHistory: function () {
        var menuNo = 5120201;
        var url = "/RecruitManager/ChargedHistory/CashPointAdd";
        this.goPageNo(menuNo, "", "");
    },
    /*
    * 기업 > 유료이용내역 - 미입금 이동
    */
    goCorpNotPaidHistory: function () {
        var menuNo = 50702;
        var url = "/RecruitManager/ChargedHistory/NotPaidList";
        this.goPageNo(menuNo, "", "");
    },
    /*
    * 기업 > 등록패키지 내역
    */
    goCorpPackageHistory: function () {
        var menuNo = 5070102;
        var url = "/RecruitManager/ChargedHistory/PackageList";
        this.goPageNo(menuNo, "", "");
    },
    /*
    * 기업 > 이력서열람서비스 내역
    */
    goCorpResumeSearchHistory: function () {
        var menuNo = 5070103;
        var url = "/RecruitManager/ChargedHistory/SearchList";
        this.goPageNo(menuNo, "", "");
    },
    /*
    * 기업 > 알바제의SMS 내역
    */
    goCorpProposeSMSHistory: function () {
        var menuNo = 5070104;
        var url = "/RecruitManager/ChargedHistory/SMSList";
        this.goPageNo(menuNo, "", "");
    },
    /*
    * 이력서 상세 뷰 이동
    */
    goResumeView: function (rIdx, titleType) {
        titleType = (titleType == undefined || titleType == null) ? 1 : titleType;
        rIdx = (rIdx == undefined || rIdx == null) ? 0 : rIdx;
        var title = "인재정보";
        var url = "";
        //if(rIdx == parseInt(rIdx, 10)) {
        //    url = this.getHttpUrl() + "/list/gg/mon_ggb_read.asp?RIdx=" +rIdx + "&M_ID="+rIdx;
        //} else {
        //    url = this.getHttpUrl() + "/list/gg/mon_ggb_read.asp?M_ID=" +rIdx;
        //}

        url = this.getHttpUrl() + "/resume/view/gg?RIdx=" + rIdx;

        //if (titleType == 1) url += "&JKWebHeaderUse=1";

        if (this.AppNo == "17" || this.AppNo == "18") {
            if (this.AppNo == "17") {
                try {
                    if ($("#dev_c_id").val() == null || $("#dev_c_id").val() == undefined || $("#dev_c_id").val() == "") {
                        this.openPopup(url, title, "", "", titleType);
                    }
                    else {
                        this.openPopup(url, title, "", "refresh_pop=1", titleType);
                    }
                }
                catch (e) {
                    this.openPopup(url, title, "", "", titleType);
                }

            } else {
                this.openPopup(url, title, "", "", titleType);
            }
        } else {

            var reUrl = location.href;
            if (location.href.toLowerCase().indexOf("/resumesearch") >= 0) { //인재List
                if (location.href.indexOf("&scrollTop=") > 0) {
                    reUrl = location.href.substring(0, location.href.indexOf("&scrollTop="));
                }
                if (location.href.indexOf("?scrollTop=") > 0) {
                    reUrl = location.href.substring(0, location.href.indexOf("?scrollTop="));
                }

                var sTop = (reUrl.indexOf("resumesearch?") > 0 || reUrl.indexOf("resumesearch/By-Date?") > 0 || reUrl.indexOf("resumesearch/Veteran?") > 0 || reUrl.indexOf("resumesearch/Freelancer?") > 0 || reUrl.indexOf("resumesearch/University?") > 0) ? "&" : "?";
                sTop = sTop + "scrollTop=" + $(window).scrollTop();

                location.href = url + "&reurl=" + encodeURIComponent(reUrl + sTop);
            }
            else if (location.href.toLowerCase().indexOf("/custom-talent/") > 0 || location.href.toLowerCase().indexOf("/recruitmanager/sms/smshistorylist") > 0 || location.href.toLowerCase().indexOf("/recruitmanager/gglist/open/") > 0 || location.href.toLowerCase().indexOf("/recruitmanager/gglist/scrap") > 0) {   //맞춤인재 List, 알바제의문자 사용내역
                location.href = url + "&reurl=" + encodeURIComponent(location.href);
            }
            else {
                location.href = url;
            }
        }

        //if (this.AppNo == "17" || this.AppNo == "18") {
        //    this.openPopup(url, title, "", "", titleType);
        //} else {
        //    location.href = url;
        //}
    },
    /*
    * 상품안내 > 등록 패키지
    */
    goProductGuidePackageSet: function () {
        if (this.AppNo == "17" || this.AppNo == "18") {
            this.goPageNo(51002, null);
        }
        else {
            location.href = "/RecruitManager/ProductGuide/PackageSet";
        }
    },
    /*
    * 상품안내 > 이력서 열람
    */
    goProductGuideSearch: function () {
        if ((this.AppNo == "17" && this.AppVerRP < "30007") || (this.AppNo == "18" && this.AppVerRP < "50007")) {
            this.goPageNo(51003, null);
        } else if (this.AppNo == "17" || this.AppNo == "18") {
            this.goPageNo(51101, null);
        } else {
            location.href = "/RecruitManager/Searching";
        }
    },
    /*
    * 쿠폰함
    */
    goCouponBox: function () {

        if ((this.AppNo == "17" && this.AppVerRP < "30007") || (this.AppNo == "18" && this.AppVerRP < "50007")) {
            this.goPageNo(50705, null);
        } else if (this.AppNo == "17" || this.AppNo == "18") {
            this.goPageNo(513, null);
        } else {
            location.href = "/RecruitManager/ChargedHistory/Coupon";
        }
    },
    /*
    * 맞춤인재 이동
    */
    goCustomTalent: function (tabType, param) {
        var tabName = "";

        if (tabType == 1) tabName = "open";
        else if (tabType == 2) tabName = "ready";
        else if (tabType == 3) tabName = "close";
        else {
            tabName = "open";
            tabType = 1;
        }

        var url = "/recruitmanager/Custom-Talent/" + tabName;

        if (this.AppNo == "17") {
            if ($.trim(param) != "") param = "selectmid=" + (50600 + tabType) + "&" + param
            else param = "selectmid=" + (50600 + tabType);

            this.goPageNo(506, param, "");

        } else if (this.AppNo == "18") {
            url = url + "?" + param;
            location.href = "toapp:page_move[&]code[=]506[&]tab_no[=]1[&]url[=]" + url
        }
        else {
            location.href = url + "?" + param;
        }
    },
    /*
    * 지원자 관리 이력수 뷰 이동
    */
    goApplyResumeView: function (pType, passNo, algino, onlyView, pass_r_no) {
        pType = (pType == undefined || pType == null) ? "" : pType;
        onlyView = (onlyView == undefined || onlyView == null) ? "" : onlyView;
        pass_r_no = (pass_r_no == undefined || pass_r_no == null) ? "" : pass_r_no;
        var title = "지원자 이력서";
        //var url = this.getHttpUrl() + "/corp/apply_mng/mon_em_apply_" + (pType.toUpperCase() == "O" ? "" : "email_") + "resume_view.asp";
        //var param = "P_Type=" + pType + "&Pass_No=" + passNo + "&AL_GI_No=" + algino + "&onlyView=" + onlyView + "&refresh_pop=1";

        var url = this.getHttpUrl() + "/resume/view/applygi";
        var param = "P_Type=" + pType + "&Pass_No=" + passNo + "&AL_GI_No=" + algino + "&onlyView=" + onlyView + "&pass_r_no=" + pass_r_no;
        if (this.AppNo == "17" || this.AppNo == "18") {
            if (this.AppNo == "17") {
                this.openPopup(url + "?" + param, title, "", "refresh_pop=1");
            } else {
                this.openPopup(url + "?" + param, title, "", "");
            }
        } else {
            location.href = url + "?" + param;
        }
    },
    /*
    * 지원자 관리 이력수 뷰 이동 - 지원이력서
    */
    goApplyPassResumeView: function (pType, passNo, algino, onlyView, pass_r_no) {
        pType = (pType == undefined || pType == null) ? "" : pType;
        onlyView = (onlyView == undefined || onlyView == null) ? "" : onlyView;
        pass_r_no = (pass_r_no == undefined || pass_r_no == null) ? "" : pass_r_no;

        var title = "지원자 이력서";
        //var url = this.getHttpUrl() + "/corp/apply_mng/mon_em_apply_" + (pType.toUpperCase() == "O" ? "" : "email_") + "resume_view.asp";
        //var param = "P_Type=" + pType + "&Pass_No=" + passNo + "&AL_GI_No=" + algino + "&onlyView=" + onlyView + "&refresh_pop=1";

        var url = this.getHttpUrl() + "/resume/view/applygi";
        var param = "P_Type=" + pType + "&Pass_No=" + passNo + "&AL_GI_No=" + algino + "&onlyView=" + onlyView + "&pass_r_no=" + pass_r_no;
        if (this.AppNo == "17" || this.AppNo == "18") {
            if (this.AppNo == "17") {
                this.openPopup(url + "?" + param, title, "", "refresh_pop=1");
            } else {
                this.openPopup(url + "?" + param, title, "", "");
            }
        } else {
            location.href = url + "?" + param;
        }
    },
    /*
    * 주소검색 팝업
    */
    openRoadAddressPop: function (from, isHttps) {
        from = (from == undefined || from == null) ? "" : from;
        isHttps = (isHttps == undefined || isHttps == null) ? false : isHttps;
        var url = "/mem/mon_address_zipcode_road.asp";
        if (isHttps) url = this.getHttpUrl() + url;

        var param = "from=" + from;
        if (this.AppNo != "17" && this.AppNo != "18") {
            url += ("?" + param);
        }

        this.openPopup(url, "주소검색", "", param);
    },
    /*
    * 고객센터 > faq
    */
    opneFaq: function (param) {
        var url = "/service/faq.asp";
        if (this.AppNo == "17" || this.AppNo == "18") {
            this.openPopup(url, "FAQ", "", param, 0);
        }
        else {
            location.href = url + "?" + param;
        }
    },

    /*
    * 이력서서칭신청 > 이용방법 및 주의사항
    */
    openSearchingInfo: function () {
        var url = "/RecruitManager/Searching/Searching-Noti";
        if (this.AppNo == "17" || this.AppNo == "18") {
            this.openPopup(url, "이용방법 및 주의사항", "", "", 0);
        }
        else {
            location.href = url;
        }
    },
    /*
    * 이력서서칭신청 > 기업인증창
    */
    openSearchingStep1: function () {
        var CID = $("#searchingBadCheck").val();
        $.ajax({
            url: "/public/BadMemCheckForResumeSearching",
            type: "POST",
            xhrFields: { withCredentials: true },
            data: {
                "CID": CID,
                "BadInfoMake": 1
            },
            dataType: "json",
            error: function (textStatus) {

            },
            success: function (data, textStatus) {
                if (data.BadNameChg == "y") {
                    alert(data.AlertM);
                } else {
                    var url = "/RecruitManager/Searching/Cert-Step1";
                    if (this.AppNo == "17" || this.AppNo == "18") {
                        //this.openPopup(url, "이력서열람서비스 신청", "", "refresh_pop=1&btn_back=돌아가기&back_msg=변경사항은 자동 저장되지 않습니다. \n저장 없이 공고등록 첫화면으로 이동하시겠습니까?", 0);
                        this.openPopup(url, "이력서열람서비스 신청", "SSL", "refresh_pop=1", 0);
                    }
                    else {
                        location.href = url;
                    }
                }
            }
        });
    },
    /*
    * 이력서서칭신청 > 개인인증창
    */
    openSearchingStep2: function (id) {
        var url = "/RecruitManager/Searching/Cert-Step2/" + id;
        if (this.AppNo == "17") {
            url = url + "?back_msg=이력서 열람서비스 신청을 중단하시겠습니까?";
            //this.openPopup(url, "이력서열람서비스 신청", "", "refresh_pop=1&btn_back=돌아가기&back_msg=변경사항은 자동 저장되지 않습니다. \n저장 없이 공고등록 첫화면으로 이동하시겠습니까?", 0);

            window.android.go_PayProc("이력서열람서비스 신청", url, "", 0, "");

            //return Content(dsGuinSave.buildTrackingContent(JSBuilder.StaticStr(string.Format("window.android.go_PayProc('상품선택', '{0}', '{1}', 502,'/recruitmanager/list-ready');window.android.close_pop();", returnURL, string.Format("PRDTCode={0}&SubPRDTCode={1}", Param.PRDTCode, Param.SubPRDTCode)))));
        }
        else if (this.AppNo == "18") {
            this.openPopup(url, "이력서열람서비스 신청", "SSL", "", 0);
        } else {
            location.href = url;
        }
    },
    /*
    * 이력서서칭신청 > 상풍다시선택하기
    */
    SearchingPrdtChoiceAgain: function (url) {
        if (this.AppNo == "17" || this.AppNo == "18") {
            this.closePopup();
        }
        else {
            location.href = url;
        }
    },
    /*
    * 이력서서칭신청 > 기업정보 수정 페이지 이동
    */
    SearchingCorpInfoModify: function (url) {
        if (this.AppNo == "17" || this.AppNo == "18") {
            this.openPopup(url, "기업정보", "SSL", "", 0);
        }
        else {
            location.href = url;
        }
    },
    /*
    * 이력서서칭유료결제내역 > 유료열람한 인재보기
    */
    SearchingListtoUserList: function (url) {
        if (this.AppNo == "17") {
            this.goPageNo(50402, null);
        } else if (this.AppNo == "18") {
            location.href = "toapp:page_move[&]code[=]50402[&]tab_no[=]2"
        } else {
            //location.href = "/corp/mon_resumeinfo_list.asp?gubun=2";
            location.href = "/recruitmanager/gglist/open/pay";
        }
    },

    /*
    * 알바제의 문자 유료결제내역 > 알바제의문자 사용내역
    */
    SmshistoryList: function (url) {
        if (this.AppNo == "17" || this.AppNo == "18") {
            this.openPopup("/recruitmanager/sms/SmsHistoryList", "알바제의문자 사용내역");
        } else {
            location.href = "/recruitmanager/sms/SmsHistoryList";
        }
    },
    /*
    * 이력서 사진첩 보기
    */
    ResumePhotoView: function (rIdx, pIdx) {
        if (this.AppNo == "17" || this.AppNo == "18") {
            this.openPopup("/resume/PhotoView/Album-" + rIdx + "?idx=" + pIdx, "사진보기");
        } else {
            location.href = "/resume/PhotoView/Album-" + rIdx + "?idx=" + pIdx;
        }
    },
    /*
    * 지원이력서 사진첩 보기
    */
    PassResumePhotoView: function (pass_r_no, pIdx) {
        if (this.AppNo == "17" || this.AppNo == "18") {
            this.openPopup("/resume/PhotoView/Pass-Album-" + pass_r_no + "?idx=" + pIdx, "사진보기");
        } else {
            location.href = "/resume/PhotoView/Pass-Album-" + pass_r_no + "?idx=" + pIdx;
        }
    },
    /*
    * 스마트추천알바 보기 
    */
    goSmartRecomList: function () {
        if (this.AppNo == "17" || this.AppNo == "18") {
            this.goPageNo(122, null);
        }
    },
    /*
    * 알바제의 문자 > 공고보기
    */
    SmsSendingGuinPop: function (gi_no) {
        var url = "/recruit/view/gi?al_gi_no=" + gi_no;
        if (this.AppNo == "18" && this.AppVerRP <= "50105") {
            url = "/list/gi/mon_gib_read.asp?al_gi_no=" + gi_no;
        }

        if (this.AppNo == "17") {
            if (AM_App_Ver_RP <= "30105") {
                this.openPopup(url, "공고보기");
            } else {
                url += "&topTitle=미리보기&tmpv="
                window.android.goGibRead(gi_no, "", url);
            }
        }
        else if (this.AppNo == "18") {
            if (AM_App_Ver_RP <= "50105") {
                url = "/list/gi/mon_gib_read.asp?al_gi_no=" + gi_no;
            }
            this.openPopup(url, "공고보기");
        }
        else {
            location.href = url;
        }
    },
    /*
    * 개인 - 발신번호 표시 설정 호출
    */
    openCallDisplaySetup: function () {
        if (this.AppNo == "17") {
            if (AM_App_Ver_RP >= "30202") {
                window.android.openCallCheckSetting();
            } else {
                if (confirm("3.2.2 버전부터 사용가능합니다. \n 앱을 업데이트하시려면 [확인] 버튼을 눌러주세요.")) {
                    this.appMarket()
                }
            }
        } else if (this.AppNo == "1") {
            if (confirm("알바몬앱에서만 사용 가능합니다. \n 앱을 설치하시려면 [확인] 버튼을 눌러주세요.")) {
                this.appMarket()
            }
        }
    },

    /*
    * 기업회원 - 발신번호 사전 등록 새창 열기
    */
    openGICallDisplayRegist: function () {

        var url = "https://www.albamon.com";
        if (location.hostname.toLowerCase().indexOf("dev") >= 0) {
            url = "https://dev-www.albamon.com";
        } else if (location.hostname.toLowerCase().indexOf("test") >= 0) {
            url = "https://test-www.albamon.com";
        } else if (location.hostname.toLowerCase().indexOf("m19") >= 0) {
            url = "https://www19.albamon.com";
        }
        this.openPopup(url + "/Public/Reg-SendNum-CallNum?AM_App_No=" + this.AppNo, "발신번호표시 등록서비스", "", "");
    },
    /*
    * 네이티브 공고 상세검색창 띄우기
    */
    appSearchPopCanHide: function (addParam, hideMenu) {  //hideMenu : 조검검색 항목 숨김 타입 (지역 1, 업직종 2, 근무기간 3, 나이 4, 성별 5) 
        if (this.AppNo == "17") {
            if (AM_App_Ver_RP >= "30106") {
                window.android.goConditionSearch(addParam, hideMenu);
            } else if (AM_App_Ver_RP >= "20003") {
                window.android.goConditionSearch(addParam);
            } else {
                window.android.goConditionSearch();
            }
        } else if (this.AppNo == "18") {
            if (AM_App_Ver_RP >= "50106") {
                location.href = "toapp:condition_search[&]param[=]" + addParam + "[&]hideType[=]" + hideMenu;
            } else {
                location.href = "toapp:condition_search[&]param[=]" + addParam;
            }

        }
    },

    /**
    * 메뉴 실행
    **/
    goPageNo: function (menuid, param, url) {
        param = (param == undefined || param == null) ? "" : param;
        url = (url == undefined || url == null) ? "" : url;
        var menuInfoURL = this.MenuInfoURL;

        if (this.AppNo == "17") {
            if (menuid >= "400" && menuid < "500") {
                this.openPopupByMenuID(menuid);
            }
            else if (url != "") {
                if (url == "/") url = "";
                window.android.goPageNo(menuid, url, param);
            }
            else {
                window.android.goPageNo(menuid, param);
            }
        }
        else if (this.AppNo == "18") {
            if (url != "") {
                location.href = "toapp:page_move[&]code[=]" + menuid + "[&]url[=]" + url;
            } else {
                //this.openPopupByMenuID(menuid);
                location.href = "toapp:page_move[&]code[=]" + menuid;
            }
        }
        else {
            this.openPopupByMenuID(menuid);
        }
    },
    openPopupByMenuID: function (menuid, isPop) {
        isPop = (isPop == undefined || isPop == null) ? false : true;

        //메뉴 매핑 정보에 menuID 로 URL 정보를 가져와서 이동시킨다. 
        //메뉴 매핑에 대한 정보가 존재하지 않을 경우, 비동기로 호출하여 정보를 가져온다.
        var root = this;
        var _gotoMenu = function (menuNo) {
            var url = undefined;
            var title = "";
            var menuNo1depth = parseInt(String(menuNo).substr(0, 3));
            var menuNo2depth = parseInt(String(menuNo).substr(0, 5));

            for (var prob in root.MobileMenuInfo) {
                if (url != undefined) break;
                var menuElement = root.MobileMenuInfo[prob];

                if (menuElement.id == menuNo) {
                    title = menuElement.name;
                    url = menuElement.url;
                } else if (menuElement.id == menuNo1depth && menuElement.child && menuElement.child != null) {
                    for (var childIndex in menuElement.child) {
                        var menuElementChild = menuElement.child[childIndex];
                        if (menuElementChild.id == menuNo) {
                            title = menuElementChild.name;
                            url = menuElementChild.url;
                        } else if (menuElementChild.id == menuNo2depth && menuElementChild.child && menuElementChild.child != null) {
                            for (var child2depthIndex in menuElementChild.child) {
                                var menuElement2DepthChild = menuElementChild.child[child2depthIndex];
                                if (menuElement2DepthChild.id == menuNo) {
                                    title = menuElement2DepthChild.name;
                                    url = menuElement2DepthChild.url;
                                }
                                if (url != undefined) break;
                            }
                        }

                        if (url != undefined) break;
                    }
                }
            }

            if (url != undefined) {
                if (isPop == false && root.AppNo != "17" && root.AppNo != "18") {
                    location.href = url;
                } else {

                    root.openPopup(url, title, "", "", 0);
                }
            }
        }

        if (this.MobileMenuInfo == null) {
            var data = { AM_App_No: "1", App_Ver: "3.0.0" };
            $.ajax({
                type: "POST", url: this.MenuInfoURL, dataType: "json", data: data, async: true,
                error: function (e) {
                    //alert("error");
                },
                success: function (result) {
                    var menuElementList = new Array();
                    for (var index = 0; index < result.menu.length; index++) {
                        var cMenu = result.menu[index];
                        for (var childIndex = 0; childIndex < cMenu.child.length; childIndex++) menuElementList.push(cMenu.child[childIndex]);
                    }
                    root.MobileMenuInfo = menuElementList;
                    _gotoMenu(menuid);
                }
            });
        }
        else {
            _gotoMenu(menuid);
        }
    },
    /**
    * 알바몬 페이지 새창 열기
    */
    openPopup: function (url, title, type, param, titleType, backBtnName) {
        type = (type == undefined || type == null) ? "" : type;
        param = (param == undefined || param == null) ? "" : param;
        titleType = (titleType == undefined || titleType == null) ? 0 : titleType;
        backBtnName = (backBtnName == undefined || backBtnName == null) ? "" : backBtnName;

        if (this.AppNo == "17") {
            if (backBtnName != "") {
                if (url.toLowerCase().indexOf("btn_back=") == -1 && param.toLowerCase().indexOf("btn_back=") == -1) {
                    param = ((param == "") ? "" : "&") + "btn_back=" + backBtnName;
                }
            }
            window.android.openPopup(url, title, type, param, parseInt(titleType));
        } else if (this.AppNo == "18") {
            var popup_option = "";
            if (backBtnName != "") {
                popup_option = "[&]backbtn_type[=]1[&]backbtn_name[=]" + backBtnName;
            }
            this.toAppExecute("popup[&]url[=]" + url + "[&]title[=]" + title + "[&]param[=]" + param + "[&]title_type[=]" + titleType + popup_option);
        }
        else {
            window.open(url, "_blank");
        }
    },
    /**
    * 알바몬 외부 링크 새창 열기
    * 모바일에서는 Native Browser 호출
    **/
    webOpen: function (url, title) {
        if (this.AppNo == "17") {
            window.android.web_open(url);
        }
        else if (this.AppNo == "18") {
            this.toAppExecute("safari[&]url[=]" + url);
        }
        else {
            window.open(url, "_blank");
        }
    },
    /**
    * 팝업창 닫기
    **/
    closePopup: function (popupname, refresh) {
        popupname = (popupname == undefined) ? "" : popupname;
        refresh = (refresh == undefined) ? false : refresh;

        if (this.AppNo == "17") {
            if (popupname == "") {
                window.android.close_pop(popupname);
            } else {
                window.android.close_pop();
            }
        } else if (this.AppNo == "18") {
            if (refresh == true) {
                location.href = "toapp:popup_close[&]close_type[=]1";
            } else {
                location.href = "toapp:popup_close[&]close_type[=]0";
            }
        } else {
            window.close();
        }
    },
    /*
    * toapp 실행
    */
    toAppExecute: function (url) {

        var iframe = document.createElement("iframe");
        iframe.setAttribute("src", "toapp:" + url);
        document.documentElement.appendChild(iframe);
        iframe.parentNode.removeChild(iframe);
        iframe = null;
    },

    /*#######################
    * Android Bridge
    #######################*/

    AndroidExecuteScript: function (script) {
        if (script == undefined) return;
        window.android.script(script);
    },

    /*
    * 네이티브 검색 초기화하기
    */
    appSearchRest: function () {
        if (this.AppNo == "17") {
            if (AM_App_Ver_RP >= "30107") {
                window.android.clearConditionSearch();
            }
        } else if (this.AppNo == "18") {
            if (AM_App_Ver_RP >= "50107") {
                location.href = "toapp:clear_condition_search[&]param[=]";
            }
        }
    },
    appCalendar: function (type, title, desc, area, sdt, edt) {
        if (this.AppNo == "17" && this.AppVerRP >= "30107") {
            window.android.addCalendarEvent(title, desc, area, sdt, edt);

            if (type == 1) {
                window.android.showToast("캘린더에 등록한 채용정보는 스크랩 알바에서도 확인할 수 있습니다");
            }
        }
        else if (this.AppNo == "18" && this.AppVerRP >= "50107") {
            this.toAppExecute("add_calendar_event[&]type[=]" + type + "[&]title[=]" + title + "[&]description[=]" + desc + "[&]location[=]" + area + "[&]startDate[=]" + sdt + "[&]endDate[=]" + edt);
        }
    },
    alramSet: function (push_type, push_stat, vib_stat, option_v, msg) {
        try {
            if (this.AppNo == "17" && this.AppVerRP >= "30102") {
                window.android.set_push_alram(push_type, push_stat, vib_stat, option_v, msg);
            }
            else if (this.AppNo == "18" && this.AppVerRP >= "50102") {
                this.toAppExecute("set_push_alram[&]type[=]" + push_type + "[&]isOn[=]" + push_stat + "[&]isVib[=]" + vib_stat + "[&]value1[=]" + option_v + "[&]message[=]" + msg);
            }
        }
        catch (e) {
            alert(e)
        }
    },
    appMarket: function () {
        if (this.AppNo == "17") {
            window.android.web_open('market://details?id=com.albamon.app');
        } else if (this.AppNo == "18") {
            this.webOpen("https://itunes.apple.com/kr/app/id382535825", "");
        } else if (this.AppNo == "14") {
            location.href = "https://itunes.apple.com/kr/app/id382535825";
        } else if (this.AppNo == "1") {
            this.webOpen("market://details?id=com.albamon.app", "");
        }
    }

}