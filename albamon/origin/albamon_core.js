﻿/**
    * Albamon Js Common Core
    *
    * jquery 기반으로 작성하였으므로 반드시 jquery 프레임워크가 필요합니다.
**/
var $amc = AlbamonCore = {
    browser: {},
    httpContext: {}, hc: {},
    lib: {}
};

//-----------------------------------------------------------
// 브라우저 관련
//-----------------------------------------------------------
$amc.browser = {
    // 익스플로어 여부
    isIE: ($.browser.msie || ($.browser.mozilla && !!navigator.userAgent.match(/Trident.*rv\:11\./))) ? true : false,
    // 익스플로어 6.0 여부
    isIE6: ($.browser.msie && $.browser.version == '6.0') ? true : false,
    // 익스플로러 7.0 여부
    isIE7: ($.browser.msie && $.browser.version == '7.0') ? true : false,
    // 익스플로러 8.0 여부
    isIE8: ($.browser.msie && $.browser.version == '8.0') ? true : false,
    // 익스플로러 9.0 여부
    isIE9: ($.browser.msie && $.browser.version == '9.0') ? true : false,
    // 익스플로러 10.0 여부
    isIE10: ($.browser.msie && $.browser.version == '10.0') ? true : false,
    // 익스플로러 11.0 여부
    isIE11: !!navigator.userAgent.match(/Trident.*rv\:11\./),
    // 익스플로러 8.0 호환성보기 여부
    isIE8UA: function () {
        if ($.browser.msie && $.browser.version == '8.0') {
            return false;
        } else if ($.browser.msie && $.browser.version == '7.0') {
            if (navigator.userAgent.indexOf('Trident/4.0') >= 0) {
                return true;
            }
            if (navigator.userAgent.indexOf('Trident/7.0') >= 0) {
                return true;
            }
        }
        return false;
    },
    // 모바일 여부
    isMobile: ((navigator.userAgent.toLowerCase().match(/ipad/) == null && navigator.userAgent.toLowerCase().match(/iphone|mobile|up.browser|android|blackberry|windows ce|nokia|webos|opera mini|sonyericsson|opera mobi|windows phone|iemobile|polaris/) != null)) ? true : false,
    // 크롬브라우져 인지 체크
    isCrorm: ($.browser.chrome) ? true : false,
    // 사파리
    isSafari: ($.browser.safari) ? true : false,
    // 오페라
    isOpera: ($.browser.opera) ? true : false,
    // 모질라
    isMozilla: ($.browser.mozilla) ? true : false
};

//-----------------------------------------------------------
// 접속 단말기 관련
//-----------------------------------------------------------
$amc.device = {
    name: function () {
        var result = '';
        var mobileKeyWords = new Array('iPhone', 'iPod', 'iPad', 'BlackBerry', 'Android', 'Windows CE', 'LG', 'MOT', 'SAMSUNG', 'SonyEricsson', 'X11');

        for (var i = 0; i < mobileKeyWords.length; i++) {
            if (navigator.userAgent.match(mobileKeyWords[i]) != null) {
                result = mobileKeyWords[i];
                break;
            }
        }

        return result;
    }
};

//-----------------------------------------------------------
// httpContext 관련
//-----------------------------------------------------------
$amc.httpContext.request = {
    getCurrentHostUrl: function () {
        var result = window.location.protocol + "//" + window.location.hostname;
        return result;
    },
    isMobileDomain: function () {
        var str = this.getCurrentHostUrl().toLowerCase();
        return /^(http|https)\:\/\/(test-m|dev-m|(m(|(\d{1})|(\d{2})|(\d{3}))))\.albamon.com/g.test(str);   // 알바몬 모바일 전체 사이트 정규식 추출 (개발,테섭,실섭 모두 포함)
    },
    // 개발서버 여부 
    isDev: function () {
        if (this.isMobileDomain()) {
            return this.getCurrentHostUrl().toLowerCase().indexOf("://dev-m.albamon.com") > -1;
        } else {
            return this.getCurrentHostUrl().toLowerCase().indexOf("://dev-www.albamon.com") > -1;
        }
    },
    // 테스트서버 여부
    isTest: function () {
        if (this.isMobileDomain()) {
            return this.getCurrentHostUrl().toLowerCase().indexOf("://test-m.albamon.com") > -1;
        } else {
            return this.getCurrentHostUrl().toLowerCase().indexOf("://test-www.albamon.com") > -1;
        }
    },
    isStaging: function () {
        if (this.isMobileDomain()) {
            return this.getCurrentHostUrl().toLowerCase().indexOf("://m19.albamon.com") > -1;
        } else {
            return this.getCurrentHostUrl().toLowerCase().indexOf("://www19.albamon.com") > -1 || this.getCurrentHostUrl().toLowerCase().indexOf("://www146.albamon.com") > -1;
        }
    },
    getDomain: function () {
        var domain = ".albamon.com";
        var first = "";
        var deviceCode = "";

        if (this.isDev()) {
            first = "dev-";
        } else if (this.isTest()) {
            first = "test-";
        } else if (this.isStaging()) {
            first = "19";
        }

        if (this.isMobileDomain()) {
            deviceCode = "m";
        } else {
            deviceCode = "www";
        }

        domain = this.isStaging() ? deviceCode + first + domain : first + deviceCode + domain;

        return domain;
    }
};
$amc.hc.req = $amc.httpContext.request; // short rename

//-----------------------------------------------------------
// 페이지 관련
//-----------------------------------------------------------
$amc.page = {
    go: function (url, target, title) {
        if (url == null || url == "") return false;

        if ($amc.browser.isMobile) {
            if (AM_App_No == "17") {
                if (target == "_blank") {
                    window.android.openPopup(url, title);
                } else {
                    location.href = url;
                }
            } else if (AM_App_No == "18") {
                if (url.toLowerCase().indexOf("am_app_no") < 0) {
                    if (AM_App_No != null) {
                        (url.indexOf("?") >= 0) ? url += "&" : url += "?";
                        url += ("AM_App_No=" + AM_App_No);
                    }
                }
                if (target == "_blank") {
                    var splitUrl = url.split("?");
                    var _url = "toapp:popup[&]url[=]" + splitUrl[0];

                    if (title != "" && title != undefined) {
                        _url += "[&]title[=]" + title;
                    }

                    if (splitUrl.length > 1) {
                        if (splitUrl[1] != "" && splitUrl[1] != undefined) {
                            _url += "[&]param[=]" + splitUrl[1];
                        }
                    }

                    location.href = _url;
                } else {
                    location.href = "toapp:gopage[&]url[=]" + url + "[&]title[=]" + title;
                }
            } else {
                if (target == "_blank") {
                    window.open(url, target);
                } else {
                    location.href = url;
                }
            }
        } else {
            if (target == "_blank") {
                window.open(url, target);
            } else {
                location.href = url;
            }
        }
    },
    goWeb: function (url) {
        if (url == null || url == "") return false;

        if ($amc.browser.isMobile) {
            if (AM_App_No == "17") {
                window.android.web_open(url);
            } else if (AM_App_No == "18") {
                this.goHidFrame("toapp:safari[&]url[=]" + url);
            } else {
                this.go(url, "_blank");
            }
        } else {
            this.go(url, "_blank");
        }
    },
    goHidFrame: function (url) {
        var iframe = document.createElement("iframe");
        iframe.setAttribute("src", url);
        document.documentElement.appendChild(iframe);
        iframe.parentNode.removeChild(iframe);
        iframe = null;
    }
};

//-----------------------------------------------------------
// 이벤트 관련
//-----------------------------------------------------------
$amc.event = {
    stop: function (e) {
        if (typeof e.preventDefault === "function") {
            e.preventDefault();
        }
        if (typeof e.stopPropagation === "function") {
            e.stopPropagation();
        }
        // IE
        if (typeof e.returnValue === "boolean") {
            e.returnValue = false;
        }
        if (typeof e.cancelBubble === "boolean") {
            e.cancelBubble = true;
        }
    }
};

//-----------------------------------------------------------
// 인증 관련
//-----------------------------------------------------------
$amc.auth = {
    login: function (re_url) {
        if (AM_App_No == "17") {
            window.android.login_pop_onlyType(0);
        } else {
            var url = "/login/login.asp?mtype=gg&re_url=" + encodeURIComponent(re_url);
            location.href = url;
        }
    }
};

//-----------------------------------------------------------
// 유효성 관련
//-----------------------------------------------------------
$amc.validator = {
    // 검사방법들
    types: {},
    // 오류메시지들
    messages: [],
    // 타겟들
    targets: [],
    // 실행함수
    callbacks: [],
    // 데이터에 대한 유효성 검사 설정
    config: {},
    // 검사 인터페이스
    validate: function (data, target) {
        var i, msg, type, checker, result_ok, target, callback;
        
        this.messages = []; // 기존 오류메시지 초기화
        this.targets = [];      // 기존 타겟객체들 초기화
        this.callbacks = [];    // 기존 실행함수들 초기화

        for (i in data) {
            if (data.hasOwnProperty(i)) {
                type = this.config[i];
                checker = this.types[type];

                if (!type) {
                    continue;   // 설정된 검사가 없으면 넘김
                }
                if (!checker) {
                    // 설정이 존재하나 검사방법을 찾을 수 없을 경우 오류 발생
                    throw {
                        name: "ValidationError",
                        message: type + "값을 처리할 유효성 검사기가 존재하지 않습니다."
                    };
                }
                result_ok = checker.validate(data[i]);
                if (!result_ok) {
                    //msg = "\"" + i + "\" 값이 유효하지 않습니다. " + checker.instructions;
                    msg = checker.instructions;
                    this.messages.push(msg);
                    // 유효성실패 시 타겟 담기
                    target = checker.target;
                    if (typeof (target) != "undefined") {
                        this.targets.push(target);
                    } else {
                        this.targets.push(null);
                    }
                    // 유효성실패 시 실행함수 담기
                    callback = checker.callback;
                    if (typeof (callback) != "undefined") {
                        this.callbacks.push(callback);
                    } else {
                        this.callbacks.push(null);  // 배열인덱스를 맞추기 위해 null 삽입
                    }
                }
            }
        }
        return this.hasErrors();
    },

    hasErrors: function () {
        return this.messages.length !== 0;
    }
};
// 필수여부
$amc.validator.types.isEmpty = {
    validate: function (val) {
        if (val == undefined || val == null || val == "") {
            return false;
        }
        return true;
    },
    instructions: "이 값은 필수입니다."
};
// 날짜형식 체크
$amc.validator.types.isDate = {
    validate: function (val) {
        var parms = val.split(/[\.\-\/]/);
        var yyyy = parseInt(parms[0], 10);
        if (yyyy < 1900 || 3000 < yyyy) {
            return false;
        }

        var mm = parseInt(parms[1], 10);
        if (mm < 1 || mm > 12) {
            return false;
        }

        var dd = parseInt(parms[2], 10);
        if (dd < 1 || dd > 31) {
            return false;
        }

        var dateCheck = new Date(yyyy, mm - 1, dd);
        return (dateCheck.getDate() === dd && (dateCheck.getMonth() === mm - 1) && dateCheck.getFullYear() === yyyy);
    },
    instructions: "올바르지 않는 날짜 형식입니다."
};
// 숫자형식 체크
$amc.validator.types.isNumber = {
    validate: function (val) {
        var pattern = /[^(0-9)]/gi;
        return !pattern.test(val);
    },
    instructions: "숫자만 사용 가능합니다."
};
// 문자형식 체크
$amc.validator.types.isText = {
    validate: function (val) {
        var pattern = /[^(가-힣ㄱ-ㅎㅏ-ㅣa-zA-Z/\s/g 0-9)]/gi;
        return !pattern.test(val);
    },
    instructions: "특수문자를 제외한 글자와 숫자만 가능합니다."
};
// 연락처 형식 체크 
$amc.validator.types.isPhone = {
    validate: function (val) {
        var pattern = /(^02.{0}|^01.{1}|[0-9]{3})-([0-9]+)-([0-9]{4})$/;
        return pattern.test(val);
    },
    instructions: "올바르지 않는 전화번호입니다."
};
// 메일 형식 체크
$amc.validator.types.isEmail = {
    validate: function (val) {
        var pattern = /^[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[@("@")]{1}[-A-Za-z0-9_]+[-A-Za-z0-9_.]*[.]{1}[A-Za-z]{2,5}$/;
        return pattern.test(val);
    },
    instructions: "올바르지 메일형식 입니다."
};

//-----------------------------------------------------------
// 포멧 관련
//-----------------------------------------------------------
$amc.format = {
    comma: function (val) {
        val = val + "";
        return val.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
    },
    uncomma: function (val) {
        val = val + "";
        return val.replace(/[^\d]+/g, '');
    }
};

//-----------------------------------------------------------
// cookie 관련
//-----------------------------------------------------------
$amc.cookie = {
    //쿠키값 얻기
    getCookie: function (name) {
        var nameStr = name + "=";
        var nameLen = nameStr.length;
        var cookieLen = document.cookie.length;     //쿠기값이 없을시 기본적으로 45이다.  document.cookie.length >= 45

        //a로 지정시 : document.cookie ==> lucya=a; ASPSESSIONIDQGQQGLDC=GKDDHCPDJBOBAONCMJLHBCCN
        var i = 0;
        while (i < cookieLen) {
            var j = i + nameLen;
            if (document.cookie.substring(i, j) == nameStr) {
                var end = document.cookie.indexOf(";", j); // ;의 위치     
                if (end == -1) end = document.cookie.length;
                return unescape(document.cookie.substring(j, end)); //쿠키값 반환
            }
            i = document.cookie.indexOf(" ", i) + 1;
            if (i == 0) {
                break;
            }
        }
    },
    //쿠키값 저장
    setCookie: function (name, value) {
        var expires = new Date();
        var path, domain, secure;

        //호출하는 인수의 배열 setCookie.arguments[0] ~ [setCookie.arguments.length-1]
        var argv = this.setCookie.arguments;

        //호출한함수의 인수 -  setCookie.arguments.length = arguments.length와 값이 같음!!, 
        //대조 : setCookie.length 호출당하는 인수 갯수 
        var argc = this.setCookie.arguments.length;

        if (argc > 2 && argv[2] != null) {
            expires.setTime(expires.getTime() + (1000 * 60 * argv[2])); // argv[2]분동안 쿠키 유효
        }
        else {
            expires = null;
        }

        path = (argc > 3) ? argv[3] : null;
        domain = (argc > 4) ? argv[4] : null;
        secure = (argc > 5) ? argv[5] : false;

        document.cookie = name + "=" + escape(value) +
                ((expires == null) ? "" : (";expires=" + expires.toGMTString())) +
                ((path == null) ? "" : (";path=" + path)) +
                ((domain == null) ? "" : (";domain=" + domain)) +
                ((secure == true) ? " ;secure" : "");
    },
    //쿠키삭제
    delCookie: function (name) {
        var today = new Date();
        var path, domain;
        today.setTime(today.getTime() - 1);

        var argv = this.delCookie.arguments;
        var argc = this.delCookie.arguments.length;

        if (argc > 1 && argv[1] != null) {
            path = argv[1];
        }
        if (argc > 2 && argv[2] != null) {
            domain = argv[2];
        }

        var value = this.getCookie(name);
        if (value != null) {
            var vv = name + "=" + escape(value) + ("; expires=" + today.toGMTString());
            if (path != "") {
                vv += "; path=" + path;
            }
            if (domain != "") {
                vv += "; domain=" + domain;
            }
            document.cookie = vv;   //쿠키삭제  
            return (value + " 삭제완료");
        }
        else {
            return ("존재하지 않음");
        }
    }
};
//-----------------------------------------------------------
// 팝업 관련
//-----------------------------------------------------------
var objWindowList = []; //윈도우창 리스트
$amc.window = {
    popup: function (_url, _winname, _width, _height, _opt) {
        try {
            //윈도우창 검색
            var oAddWin = this.findAddWindow(_winname);
            var oAddWinNo = oAddWin.getWinNo();
            var oAddWinObj = oAddWin.getWinObj();
            //팝업이 이미 떠 있으면 닫기
            if (oAddWinNo != "") {
                oAddWinObj.close();
            }
            //팝업 위치
            var left_ = 0;
            var top_ = 0;
            //옵션값이 없으면 화면가운데 위치
            if (typeof (_opt) == "undefined") {
                _opt = "";
            }
            if (_opt.indexOf("left") == -1) {
                left_ = screen.width;
                left_ = left_ / 2 - (_width / 2);
                _opt += ',left=' + left_;
            }
            if (_opt.indexOf("top") == -1) {
                top_ = screen.height;
                top_ = top_ / 2 - (_height / 2);
                _opt += ',top=' + top_;
            }

            if (_opt.substring(0, 1) != ",") { _opt = "," + _opt; }

            //윈도우창 객체 인스턴스화
            var addWindow = new this.objWindow();
            addWindow.setWinNo(_winname);
            addWindow.setWinObj(window.open(_url, _winname, 'height=' + _height + ',width=' + _width + _opt));
            addWindow.winObj.focus();
            // 윈도우창 리스트에 담기
            objWindowList.push(addWindow);
        } catch (e) { }
    },
    objWindow: function () {    //윈도우창 객체 생성
        this.winNo = "";
        this.winObj = "";

        this.getWinNo = function () { return this.winNo; }
        this.setWinNo = function (winNo) { this.winNo = winNo; }

        this.getWinObj = function () { return this.winObj; }
        this.setWinObj = function (winObj) { this.winObj = winObj; }

        this.closeWinObj = function () { if (this.winObj != "") { this.winObj.close(); } }
    },
    findAddWindow: function (_winNo) {  //윈도우창 검색
        var tmpAddWindow = new this.objWindow();
        for (i = 0; i < objWindowList.length; i++) {
            if (objWindowList[i].getWinNo() == _winNo) {
                tmpAddWindow = objWindowList[i];
            }
        }
        return tmpAddWindow;
    },
    allWindowClose: function () {   //모든 윈도우창 닫기
        for (i = 0; i < objWindowList.length; i++) {
            objWindowList[i].closeWinObj();
        }
    },
    closeWindow: function (_winname) {  // 윈도우창 닫기
        //윈도우창 검색
        var oAddWin = this.findAddWindow(_winname);
        var oAddWinObj = oAddWin.getWinObj();
        //팝업이 이미 떠 있으면 닫기
        if (oAddWinObj != "") {
            oAddWinObj.close();
        }
    }
};