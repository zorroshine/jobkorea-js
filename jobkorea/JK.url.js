/*
    필수 참조 파일 : 
        JK.extend.js    --모듈 확장
        JK.regEx.js --정규식
        JK.pathInfo.js  --도메인 여부

*/
/*
    var extend = function() {
        var extended = {};
        for(key in arguments) {
            var argument = arguments[key];
            for (prop in argument) {
            if (Object.prototype.hasOwnProperty.call(argument, prop)) {
                extended[prop] = argument[prop];
            }
            }
        }
        return extended;
    };
*/

//JK.pathInfo = extend((JK.url || {}), factory(JK.url));

JK.url = {
    isPC : JK.pathInfo.isPC(),
    status : JK.pathInfo.getStatus(),
    
    file1Domain :  function() {
        if (this.status === "stg" || this.status === "real") {
            return "file1.jobkorea.co.kr";
        } else {
            return "test-file1.jobkorea.co.kr";
        }
    },

    file2Domain : function() {
        if (this.status === "stg" || this.status === "real") {
            return "file2.jobkorea.co.kr";
        } else {
            return "test-file2.jobkorea.co.kr";
        }
    },

    fileCoDomain : function() {
        if (this.status === "stg" || this.status === "real") {
            return "fileco.jobkorea.co.kr";
        } else {
            return "test-fileco.jobkorea.co.kr";
        }
    },

    contentsDomain : function() {
        if (this.status === "real") {
            return "i.jobkorea.kr";
        } else {
            return "its.jobkorea.kr";
        }
    },

    payDomain : function() {
        var domain = "pay.jobkorea.co.kr";
        var mUrl = isPC? "" : "/m";

        if (this.status === "real") {
        } else if (this.status === "stg" || this.status === "pre") {
            domain = "pay104.jobkorea.co.kr";
        } else {
            domain = "test.pay.jobkorea.co.kr";
        }

        return domain + mUrl;
    }
}